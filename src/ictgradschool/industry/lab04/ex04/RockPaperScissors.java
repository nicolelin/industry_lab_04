package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */

public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.

    public static final int SCISSORS = 2;

    public static final int PAPER = 3;

    public static final int QUIT = 4;

    public void start() {

        /// START METHOD TO CALL OTHER METHODS ///

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.

        String playerName = getPlayerName(); // PLAYER NAME

        int c = 1;

        System.out.println("Let's play a game!" + '\n');

        while (c > 0) {

            System.out.println("Your choices are:");
            System.out.println("1. Rock");
            System.out.println("2. Scissors");
            System.out.println("3. Paper");
            System.out.println("4. Quit");

            System.out.println();

            int computerChoice = getRandomFromRange(1, 3);

            int playerChoice = getPlayerChoice();

            if (playerChoice == 4) {
                System.out.println("Goodbye, " + playerName + ". Thanks for playing :)");
                break;
            }

            System.out.println(); // LINE BREAK

            // Grab choice from getPlayerChoice()
            // Print choice
            System.out.println(playerName + "'s choice: " + choiceString(playerChoice));
            // DEBUG
            // System.out.println(playerChoice + '\n');

            // Grab computer choice from generateRandomNumber()
            // Print choice
            System.out.println("Computer's choice: " + choiceString(computerChoice));
            // DEBUG
            // System.out.println(computerChoice + '\n');

            System.out.println();

            boolean winning = userWins(playerChoice, computerChoice);

/*            String result = getResultString(playerChoice, computerChoice);

            System.out.println(result); */

            System.out.println(getResultString(playerChoice, computerChoice));

            if (playerChoice == computerChoice) {
                System.out.println("No one wins!");
            } else if
                    (userWins(playerChoice, computerChoice)) {
                System.out.println("You win!");
            } else {
                System.out.println("You lost!");
            }

            System.out.println('\n' + "Do you want to play again? (Y/N)");
            String playAgain = Keyboard.readInput();

            if (playAgain.equals("N") || playAgain.equals("n")) {
                c = -1;
                System.out.println("Goodbye, " + playerName + ". Thanks for playing :)");
            }

        }

    }

    /// METHODS ///

    // RANDOM NUMBER GENERATOR // *CUSTOM METHOD

    public int getRandomFromRange(int min, int max) {
        int random = (int) (Math.random() * (max - min + 1) + min);

        // DEBUG
        // System.out.println("Computer's choice: " + random);

        return random;
    }

    // GET PLAYER NAME FROM INPUT // *CUSTOM METHOD

    public String getPlayerName() {

        System.out.println("Hi! What is your name?");
        String name = Keyboard.readInput();

        // DEBUG
        System.out.println('\n' + "Thanks! Nice to meet you, " + name + "." + '\n');

        return name;
    }

    // DISPLAY CHOICE // NOT USED

    /*public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)

        // DISPLAY CHOICES

        System.out.println("Let's play a game!" + '\n');

        System.out.println("Your choices are:");
        System.out.println("1. Rock");
        System.out.println("2. Scissors");
        System.out.println("3. Paper");
        System.out.println("4. Quit");

        System.out.println("");

    }*/

    // GET PLAYER CHOICE FROM INPUT // *CUSTOM METHOD

    public int getPlayerChoice() {

        // Set and initialise choice
        int choice;

        // Prompt user to enter choice
        System.out.print("Enter your choice: ");
        choice = Integer.parseInt(Keyboard.readInput());

        // Is there a away to check if the user input is valid?

        // DEBUG
        // System.out.println("Your choice is: " + choice);

        return choice;
    }

    // Make system convert the int for choice to its corresponding value in String

    public String choiceString(int choice) {
        if (choice == 1) {
            return "Rock";
        }
        if (choice == 2) {
            return "Scissors";
        }
        if (choice == 3) {
            return "Paper";
        }
        if (choice == 4) {
            return "Quit";
        }
        return "";
    }


    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.

        if (playerChoice == ROCK) {
            if (computerChoice == ROCK) {
                // System.out.println("Rock vs Rock: TIE");
                return false;
            } else if (computerChoice == PAPER) {
                // System.out.println("Rock vs Paper: YOU LOSE");
                return false;
            } else if (computerChoice == SCISSORS) {
                // System.out.println("Rock vs Scissors: YOU WIN");
                return true;
            }

        } else if (playerChoice == SCISSORS) {
            if (computerChoice == ROCK) {
                // System.out.println("Scissors vs Rock: YOU LOSE");
                return false;
            } else if (computerChoice == PAPER) {
                // System.out.println("Scissors vs Paper: YOU WIN");
                return true;
            } else if (computerChoice == SCISSORS) {
                // System.out.println("Scissor vs Scissor: TIE");
                return false;
            }

        } else if (playerChoice == PAPER) {
            if (computerChoice == ROCK) {
                // System.out.println("Paper vs Rock: YOU WIN");
                return true;
            } else if (computerChoice == PAPER) {
                // System.out.println("Paper vs Paper: TIE");
                return false;
            } else if (computerChoice == SCISSORS) {
                // System.out.println("Paper vs Scissors: YOU LOSE");
                return false;
            }
        }

        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "Paper covers rock";
        final String ROCK_WINS = "Rock smashes scissors";
        final String SCISSORS_WINS = "Scissors cut paper";
        final String TIE = "You chose the same as the computer.";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.

        if (playerChoice == PAPER && computerChoice == ROCK || playerChoice == ROCK && computerChoice == PAPER)
            return PAPER_WINS;

        if (playerChoice == ROCK && computerChoice == SCISSORS || playerChoice == SCISSORS && computerChoice == ROCK)
            return ROCK_WINS;

        if (playerChoice == SCISSORS && computerChoice == PAPER || playerChoice == PAPER && computerChoice == SCISSORS)
            return SCISSORS_WINS;

        if (playerChoice == computerChoice)
            return TIE;

    return null;
    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
