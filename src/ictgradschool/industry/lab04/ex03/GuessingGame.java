package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.

        int goal = (int) ((Math.random() * 100) + 1); // Random number generator
        int guess = 0; // Initialise and set guess variable

        System.out.println("The goal is: " + goal); // DEBUG - print goal

        System.out.println("Enter your guess (1 – 100):");

/**        guess = Integer.parseInt(Keyboard.readInput());

        while (guess != goal) {

            if (guess > goal) {
                System.out.println("Too high, try again");
                guess = Integer.parseInt(Keyboard.readInput());

            } if (guess < goal) {
                System.out.println("Too low, try again");
                guess = Integer.parseInt(Keyboard.readInput());

            } if (guess==goal) {
                System.out.println("Perfect!");
                System.out.println("Goodbye!");
            }
        } **/


        while (guess != goal) { // While guess does not equal generated number
            guess = Integer.parseInt(Keyboard.readInput());

            if (guess > goal) { // If guess is higher than generated number
                System.out.println("Too high, try again");

            } else if (guess < goal) { // if guess is lower than generated number
                System.out.println("Too low, try again");

            } /** else if (guess==goal) {
                System.out.println("Perfect!");
                System.out.println("Goodbye!");
            } **/
        }

//Exits the while loop and ends program
            System.out.println("Perfect!");
            System.out.println("Goodbye!");

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
