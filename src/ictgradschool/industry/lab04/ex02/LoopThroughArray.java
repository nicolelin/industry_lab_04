package ictgradschool.industry.lab04.ex02;

/**
 * Created by ylin183 on 14/03/2017.
 */
public class LoopThroughArray {

    /*
    private void start() {

        double Total = getTotal();

    }
    */

        private double getTotal ( double[] values){

            // (You complete this for loop)

            double totalValue = 0;

            for (int i = 0; i < values.length; i++) {

                totalValue = totalValue + values[i];

            }

            // DEBUG:
            System.out.println(totalValue);

            return totalValue;

        }

    /*
    public static void main(String[] args) {

        LoopThroughArray ex = new LoopThroughArray();
        ex.start();

    }
    */

}
