package ictgradschool.industry.lab04.ex05;

/**
 * Created by ylin183 on 21/03/2017.
 */
public class Pattern {

    // Declare the fields

    int numOfChars;
    char symbol;

    public Pattern (int numOfChars, char symbol) {
        // Constructor
        this.numOfChars = numOfChars; // Intialise fields
        this.symbol = symbol;
    }

    // Implement getters and setters

    public int getNumberOfCharacters() {
        return numOfChars;
    }

    public void setNumberOfCharacters(int numOfChars) {
        this.numOfChars = numOfChars;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public String toString() { // Using fields, convert the whole object into String
        String line = "";
        for (int i = 0; i < numOfChars; i++) {
            line += symbol;
        }
        return line;
    }

}
