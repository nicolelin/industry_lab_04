package ictgradschool.industry.lab04.ex06;

import ictgradschool.industry.lab04.ex05.Pattern;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    // brand
    public String brand;

    // model
    public String model;

    // price
    double price;
    
    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here
    public String getModel() {
        return model;
    }
    
    // TODO Insert setModel() method here
    public void setModel(String model) {
        this.model = model;
    }
    
    // TODO Insert getPrice() method here
    public double getPrice() {
        return price;
    }
    
    // TODO Insert setPrice() method here
    public void setPrice(double price) {
        this.price = price;
    }
    
    // TODO Insert toString() method here
    public String toString() {
        String phone = brand + ' ' + model + " which cost " + '$' + price + '.';
        return phone;
    }

    // TODO Insert isCheaperThan() method here
    // Compare the price of this phone with other phone
    // return true if this phone is cheaper than other phone
    // If it's true that their phone is cheaper than another person's phone, they want the other's phone

    public boolean isCheaperThan(MobilePhone other) {
        return (this.price < other.price);
    }
    
    // TODO Insert equals() method here
    // Compare the model of this phone with other phone

    public boolean equals(MobilePhone other) {
        if (this.model.equals(other.model)) {
//            System.out.println("##"+this.model);
//            System.out.println("##"+other.model);
            return true;
        } else {
//            System.out.println("@@"+this.model);
//            System.out.println("@@"+other.model);
            return false;
        }
    }

}


